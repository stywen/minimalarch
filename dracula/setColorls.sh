#!/bin/sh
mkdir -p ~/.config/colorls
cp ~/Git/repos/dracula-stywen/Theme/dracula/dracula-colorls/dark_colors.yaml ~/.config/colorls
echo "alias ls=\"colorls --dark\"" >> ~/.zshrc
echo "alias lsa=\"colorls -alh --dark\"" >> ~/.zshrc
source ~/.zshrc
