#!/bin/sh
sudo cp -r ~/Git/repos/dracula-stywen/Theme/dracula/dracula-grub/dracula /usr/share/grub/themes/dracula
echo "GRUB_THEME=\"/usr/share/grub/themes/dracula/theme.txt\"" | sudo tee -a /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg
