#!/bin/sh
~/Git/repos/minimalarch/dracula/0-getTheme.sh
~/Git/repos/minimalarch/dracula/setIcons.sh
~/Git/repos/minimalarch/dracula/setTty.sh
~/Git/repos/minimalarch/dracula/setRofi.sh
~/Git/repos/minimalarch/dracula/setI3.sh
~/Git/repos/minimalarch/dracula/setGTK.sh
~/Git/repos/minimalarch/dracula/setGrub.sh
~/Git/repos/minimalarch/dracula/setCursor.sh
~/Git/repos/minimalarch/dracula/setColorls.sh
~/Git/repos/minimalarch/dracula/setAlacritty.sh

echo "theme deployed.."
