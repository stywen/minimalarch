#!/bin/sh
yay -S nerd-fonts-jetbrains-mono
mkdir ~/.config/alacritty
cd ~/.config/alacritty
wget https://raw.githubusercontent.com/alacritty/alacritty/master/alacritty.yml
echo "import:" >> ~/.config/alacritty/alacritty.yml
echo "   - ~/Git/repos/dracula-stywen/Theme/dracula/dracula-alacritty/dracula.yml" >> ~/.config/alacritty/alacritty.yml

#setup font and fontsize in alacritty
yay -S nerd-fonts-jetbrains-mono

sed -i 's/#font:/font:/g' ~/.config/alacritty/alacritty.yml
sed -i 's/#family: monospace/family: JetBrainsMono Nerd Font/g' ~/.config/alacritty/alacritty.yml

sed -i '0,/#normal:/{s/#normal:/normal:/}' ~/.config/alacritty/alacritty.yml
sed -i 's/#bold:/#bold:/g' ~/.config/alacritty/alacritty.yml
sed -i 's/#italic:/italic:/g' ~/.config/alacritty/alacritty.yml
sed -i 's/#bold_italic:/bold_italic:/g' ~/.config/alacritty/alacritty.yml

sed -i 's/#size: 11.0/size: 12.0/g' ~/.config/alacritty/alacritty.yml

