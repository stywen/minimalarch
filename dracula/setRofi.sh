#!/bin/sh

#set mod+d for rofi
sed -i 's/dmenu_run/"rofi -modi drun,run -show drun"/g' ~/.config/i3/config
mkdir -p ~/.config/rofi/
cp ~/Git/repos/dracula-stywen/Theme/dracula/dracula-rofi/theme/config2.rasi ~/.config/rofi/config.rasi
