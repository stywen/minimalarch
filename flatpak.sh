#!/bin/sh

flatpak install -y --noninteractive flathub com.gigitux.youp
flatpak install -y --noninteractive flathub org.telegram.desktop
flatpak install -y --noninteractive flathub org.signal.Signal
flatpak install -y --noninteractive flathub com.discordapp.Discord
flatpak install -y --noninteractive flathub com.getmailspring.Mailspring
flatpak install -y --noninteractive flathub com.spotify.Client
flatpak install -y --noninteractive flathub com.bitwarden.desktop
flatpak install -y --noninteractive flathub io.freetubeapp.FreeTube
flatpak install -y --noninteractive flathub com.github.Eloston.UngoogledChromium
flatpak install -y --noninteractive flathub com.vscodium.codium
flatpak install -y --noninteractive flathub org.libreoffice.LibreOffice
flatpak install -y --noninteractive flathub org.onlyoffice.desktopeditors

