#!/bin/sh
#Author: Stywen

#set wallpaper and other i3 configuration
mkdir -p ~/Pictures/Wallpaper
mv wallpaper.png ~/Pictures/Wallpaper
echo "exec_always --no-startup-id feh --bg-fill ~/Pictures/Wallpaper/wallpaper.png" >> ~/.config/i3/config
echo "gaps inner 10" >> ~/.config/i3/config
echo "gaps outer 5" >> ~/.config/i3/config
echo "exec_always --no-startup-id picom --experimental-backend" >> ~/.config/i3/config
clear

#update if available
sudo pacman -Syyuu

#delete unnecessary packages and dependencies
sudo pacman -Rns $(pacman -Qqtd)

#clean cach
sudo pacman -Sc

echo "Finished the setup B.T.W."
sleep 5
reboot
