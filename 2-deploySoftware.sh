#!/bin/sh

sudo chown -R $USER /home/$USER/Git/ && mv ~/minimalarch/ /home/$USER/Git/repos/
#install yay
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
sudo chown -R $USER ./yay-git
cd yay-git
makepkg -si
cd
clear

#install packages
echo | sudo pacman -S i3 xorg xorg-xinit firefox
echo | sudo pacman -S alacritty zsh tree thunar rofi mtools mlocate lxappearance feh wget curl aria2 flatpak
sudo cp /etc/X11/xinit/xinitrc ~/.xinitrc
sudo nano ~/.xinitrc
sudo updatedb
clear

#optional packages
yay -S paleofetch --noconfirm --answerdiff=None
yay -S ly --noconfirm --answerdiff=None
yay -S ruby-colorls --noconfirm --answerdiff=None
yay -S font-manager --noconfirm --answerdiff=None
yay -S dmenu --noconfirm --answerdiff=None
yay -S picom-jonaburg-git --noconfirm --answerdiff=None
clear

#create Git directories
mkdir -p ~/Git/{installs,repos}

#enable displaymanager "ly"
sudo systemctl enable ly.service
sudo updatedb
clear

echo "Finished 3-software.sh"

