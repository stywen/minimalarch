#!/usr/bin/env bash
#Author: Styvven

git clone https://aur.archlinux.org/polybar.git
cd polybar
yes | makepkg -si
cd ..
mkdir ~/.config/polybar
sudo cp /usr/share/doc/polybar/config ~/.config/polybar/config
sudo chown $USER:$USER ~/.config/polybar/config
sudo cp polybarlaunch.sh ~/.config/polybar/launch.sh
sudo chmod +x ~/.config/polybar/launch.sh
echo -e ""
echo "exec_always --no-startup-id ~/.config/polybar/launch.sh" >> ~/.config/i3/config
echo -e "Please dont forget to disable the i3 bar in ~/.config/i3/config"
read -r 'entering the config file for i3...'
nano ~/.config/i3/config
