#!/bin/sh
welcome () {
    clear
    echo "==================================================="
    echo "=                                                 ="
    echo "=     Custom ARCH Deployment script               ="
    echo "=                                                 ="
    echo "=     UEFI Install                                ="
    echo "=                                                 ="
    echo "=     Author: Stywen                              ="
    echo "=     https://gitlab.com/stywen/minimalarch       ="
    echo "=                                                 ="
    echo -e "=================================================== \n"
}

setuptimeandhost () {
    echo $hostname > /mnt/etc/hostname
    echo "127.0.0.1 localhost" >> /mnt/etc/hosts
    echo "::1       localhost" >> /mnt/etc/hosts
    echo "127.0.1.1 arch.localdomain    arch" >> /mnt/etc/hosts
    echo "KEYMAP=us" > /mnt/etc/vconsole.conf
    echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf
    echo "en_US.UTF-8 UTF-8" >> /mnt/etc/locale.gen
    arch-chroot /mnt locale-gen


    ln -sf /mnt/usr/share/zoneinfo/Europe/Vienna /mnt/etc/localtime
    hwclock --systohc --localtime
    genfstab -U -p /mnt > /mnt/etc/fstab
}

setupkernal () {

    #only mandatory if system will be installed on an nvme drive
    sed -i "s/MODULES=()/MODULES=(nvme)/g" /mnt/etc/mkinitcpio.conf

}

rtpwd () { 
    clear
    echo -e "\n"
    read -p "Type your root password, be exact, and press Enter: " RTPWD
    [[ -z "$RTPWD" ]] && rtpwd
    clear
}

USRNAME=""

usrname () { 
    clear
    echo -e "\n"
    read -p "Type your user name, be exact, and press Enter: " USRNAME
    [[ -z "$USRNAME" ]] && usrname
    clear
}

usrpwd () { 
    clear
    echo -e "\n"
    read -p "Type your user password, be exact, and press Enter: " USRPWD
    [[ -z "$USRPWD" ]] && usrpwd
    clear
}

sysusrpwd () {
    clear
    arch-chroot /mnt useradd -mU -s /bin/bash -G wheel,audio,video "${USRNAME}"
    arch-chroot /mnt chpasswd <<< ""${USRNAME}":"${USRPWD}""
    arch-chroot /mnt chpasswd <<< "root:"${RTPWD}""
    #add to sudoers
    arch-chroot /mnt sed -i "0,/root ALL=(ALL) ALL/{s/root ALL=(ALL) ALL/root ALL=(ALL) ALL\n$USRNAME ALL=(ALL) ALL/}" /etc/sudoers
    clear
}

recompilekernal () {
    arch-chroot /mnt mkinitcpio -p linux
}

installpkgs () {
    clear
    pacstrap /mnt dhcpcd networkanager
    pacstrap /mnt nano vim dosfstools mtools base-devel linux-firmware usbutils #dialog dmidecode lsof  bash-completion pacman-contrib
    pacstrap /mnt zip unzip
    pacstrap /mnt rsync speedtest-cli 
    pacstrap /mnt linux linux-headers linux-docs
    pacstrap /mnt networkmanager openssh cronie intel-ucode ntp reflector 
    pacstrap /mnt os-prober
    pacstrap /mnt pulseaudio pavucontrol bluez bluez-utils
    pacstrap /mnt mesa 
}

setmirrors  () {
    #set up mirrors
    arch-chroot /mnt echo "setting up mirrors 403 error can be ignored..."
    arch-chroot /mnt reflector -c Austria -c Germany -c Switzerland -a 12 -p https --sort rate --save /etc/pacman.d/mirrorlist

}

sysctl () {
    arch-chroot /mnt systemctl enable dhcpcd.service
    arch-chroot /mnt systemctl enable NetworkManager.service
    arch-chroot /mnt systemctl enable sshd.service
    arch-chroot /mnt systemctl enable cronie.service
    arch-chroot /mnt systenctl enable ntpd.service
    arch-chroot /mnt systemctl enable bluetooth.service

}

osprober () {
    arch-chroot /mnt os-prober
    arch-chroot /mnt echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub
    arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

}

mkgit () {
    #create Git directories
    arch-chroot /mnt mkdir -p /home/$USRNAME/Git/{installs,repos} 
    arch-chroot /mnt sudo chown -R $USERNAME:$USERNAME /home/$USERNAME/Git
}

welcome

echo -e "--------------------------------------------------------------------"
    echo -e "-------------select the destination disk for the backup-------------"
    echo -e "--------------------------------------------------------------------"
    lsblk
    echo -e ""

    count=-1
    for device in `sudo fdisk -l | sed -n '/^[/]/p' | awk '{print $1}'`; do
    count=$((count+1))
    dev[$count]=$device
    printf '%s: %s\n' "$count" "$device"
    done

    read -rp "Select volume (numbers 1-$count): " selected_number
    SUB='nvme'
    choosen1="${dev[$selected_number]}"
    choosenMin1=${choosen1::-1}
    choosenMin2=${choosenMin1::-1}

#    echo $choosen1
#    echo $choosenMin1
#    echo $choosenMin2
    
    if [[ "$choosen1" == *"$SUB"* ]]; 
    then
        #./NVMEpart.sh
        nvmex0=$choosenMin1
        nvme1=${choosenMin1}1
        nvme2=${choosenMin1}2
        nvme3=${choosenMin1}3

        echo $nvmex0
        echo $nvme1
        echo $nvme2
        echo $nvme3
        sleep 1

        echo "NVME Drive."
        

        #cfdisk $choosenMin2
        mkfs.fat $nvme1
        mkfs.ext4 $nvme2
        mkfs.ext4 $nvme3
        mount $nvme2 /mnt
        mkdir /mnt/{boot,home}
        mount $nvme1 /mnt/boot
        mount $nvme3 /mnt/home

        setupkernal
  
    else
        #./SsdHddpart.sh
        sdx=$choosenMin1
        sdx1="${sdx}1"
        sdx2="${sdx}2"
        sdx3="${sdx}3"


        echo "$sdx"
        echo "$sdx1"
        echo "$sdx2"
        echo "$sdx3"

        sleep 1 
        

        #cfdisk $sdx
        mkfs.fat $sdx1
        mkfs.ext4 $sdx2
        mkfs.ext4 $sdx3
        mount $sdx2 /mnt
        mkdir /mnt/{boot,home}
        mount $sdx1 /mnt/boot
        mount $sdx3 /mnt/home
    fi

pacstrap /mnt echo | nano git xdg-user-dirs base base-devel intel-ucode linux linux-firmware linux-headers linux-docs grub efibootmgr dhcpcd networkmanager
setuptimeandhost

if [[ "$choosen1" == *"$SUB"* ]]; 
    then
        echo | pacman -S grub efibootmgr
        arch-chroot /mnt mkdir /boot/grub
        arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --removable --recheck $choosenMin2
        arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
        arch-chroot /mnt mkinitcpio -p linux
        setupkernal
    else
        echo | pacman -S grub efibootmgr
        arch-chroot /mnt mkdir /boot/grub
        arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --removable --recheck $choosenMin1
        arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
        arch-chroot /mnt mkinitcpio -p linux    
        setupkernal

fi

sysctl
setmirrors
installpkgs
osprober
usrname
usrpwd
rtpwd
sysusrpwd
mkgit
